from pyecharts.charts import Map, Timeline
from pyecharts import options
from pyecharts import options as opts

# 1. 准数据
### 山东省
sd_c = ['青岛市','济南市','潍坊市','临沂市','烟台市','聊城市','济宁市','淄博市','菏泽市','德州市','泰安市','滨州市','威海市','东营市','枣庄市','日照市']
sd_d = [1278,1163,834,704,673,632,622,587,586,570,434,401,303,289,216,201]

### 广东省
gd_c = ['广州市','东莞市','佛山市','汕头市','惠州市','中山市','揭阳市','珠海市','江门市','潮州市','湛江市','清远市','汕尾市','肇庆市','茂名市','梅州市','韶关市','河源市','阳江市','云浮市']
gd_d = [1925,1235,936,788,548,464,420,356,354,311,274,246,237,220,205,157,127,118,108,82]

### 四川省
sc_c = ['绵阳市','南充市','广元市','德阳市','宜宾市','达州市','泸州市','广安市','乐山市','遂宁市','眉山市','巴中市','凉山彝族自治州','内江市','自贡市','资阳市','雅安市','攀枝花市','阿坝藏族羌族自治州','甘孜藏族自治州']
sc_d = [944,741,639,555,493,463,359,351,340,337,300,282,239,214,190,174,157,141,123,104]

### 浙江省
zj_c = ['杭州市','温州市','宁波市','金华市','嘉兴市','台州市','绍兴市','湖州市','丽水市','衢州市','舟山市']
zj_d = [1183,792,765,582,438,381,360,288,197,103,66]

### 贵州省
gz_c = ['贵阳市','遵义市','毕节市','黔南布依族苗族自治州','黔西南布依族苗族自治州','六盘水市','安顺市','黔东南苗族侗族自治州','铜仁市']
gz_d = [1605,887,454,414,414,381,338,291,196]

# 2. 绘制山东省地图：格式一
map1 = (
    Map(init_opts=opts.InitOpts(width="700px",height="300px",theme="blue"))
    .add('', [(i,j) for i,j in zip(sd_c,sd_d)], '山东')
    .set_global_opts(visualmap_opts=opts.VisualMapOpts(max_=4000))
)
# 3. 绘制广东省地图：格式二
map2 = (
    Map()
    .add('', [(i,j) for i,j in zip(gd_c,gd_d)], '广东')
    .set_global_opts(visualmap_opts=opts.VisualMapOpts(max_=400,is_piecewise=True))
)
# 4. 绘制四川省地图：格式二
map3 = (
    Map()
    .add('', [(i,j) for i,j in zip(sc_c,sc_d)], '四川')
    .set_global_opts(visualmap_opts=opts.VisualMapOpts(max_=400,is_piecewise=True))
)
# 5. 绘制浙江省地图：格式二
map4 = (
    Map()
    .add('', [(i,j) for i,j in zip(zj_c,zj_d)], '浙江')
    .set_global_opts(visualmap_opts=opts.VisualMapOpts(max_=400,is_piecewise=True))
)
# 6. 绘制贵州省地图：格式二
map5 = (
    Map()
    .add('', [(i,j) for i,j in zip(gz_c,gz_d)], '贵州')
    .set_global_opts(visualmap_opts=opts.VisualMapOpts(max_=400,is_piecewise=True))
)



# 4. 创建组合类对象
timeline = Timeline(init_opts=opts.InitOpts(width='720px', height='350px'))

# 5. 在组合对象中添加需要组合的图表对象
timeline.add(chart=map1, time_point="山东省地图")
timeline.add(chart=map2, time_point="广东省地图")
timeline.add(chart=map3, time_point="四川省地图")
timeline.add(chart=map4, time_point="浙江省地图")
timeline.add(chart=map5, time_point="贵州省地图")
### 设置轮播时间
timeline.add_schema(is_auto_play=True, play_interval=3000)

# 6. 渲染数据
timeline.render("城市地图轮播图.html")
